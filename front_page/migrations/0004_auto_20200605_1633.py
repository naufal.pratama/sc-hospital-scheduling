# Generated by Django 2.1.1 on 2020-06-05 09:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('front_page', '0003_auto_20200605_1629'),
    ]

    operations = [
        migrations.AlterField(
            model_name='excel',
            name='hospitals_excel',
            field=models.FileField(upload_to=''),
        ),
        migrations.AlterField(
            model_name='excel',
            name='staffs_excel',
            field=models.FileField(upload_to=''),
        ),
    ]
