from django import forms
from .models import Excel

class Excel_Form(forms.ModelForm):

	class Meta:
		model = Excel
		fields = ['staffs_excel', 'hospitals_excel']
		labels = {'staffs_excel':'Upload Daftar Perawat', 'hospitals_excel':'Upload Daftar Rumah Sakit'}
