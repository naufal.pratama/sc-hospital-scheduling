from django.shortcuts import render
from scheduling_app.genetic import genetic_algorithm
from .forms import Excel_Form
from .models import Excel
import pandas as pd
from django.http import HttpResponseRedirect

# Create your views here.

def index(request):
	response = {}
	return render(request,'front_page.html',response)
	
def scheduledapp(request):
	days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
	new_excel = Excel.objects.all()[0]
	list_of_staffs = pd.read_excel(new_excel.staffs_excel).values.tolist()
	list_of_hospitals = pd.read_excel(new_excel.hospitals_excel).values.tolist()
	staffs = [s[0] for s in list_of_staffs][:20]
	hospitals = [h[0] for h in list_of_hospitals][:20]
	form = Excel_Form()
	# staffs = genetic_algorithm(staffs, hospitals, 1)
	return render(request, 'scheduleapp.html', {'form': form, 'staffs': staffs, 'hospitals': hospitals})

def scheduleapp(request):
	if request.method == 'POST':
		Excel.objects.all().delete()
		form = Excel_Form(request.POST, request.FILES)
		if form.is_valid():
		    # file is saved
		    form.save()  
		    return HttpResponseRedirect('/scheduledapp')
	else:
	    form = Excel_Form()
	return render(request, 'scheduleapp.html', {'form': form})

	