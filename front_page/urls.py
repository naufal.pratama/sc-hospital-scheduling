from django.urls import path, include
from front_page.views import index, scheduleapp, scheduledapp

urlpatterns = [
    path('', index, name='index'),
	path('scheduleapp/', scheduleapp, name='scheduleapp'),
	path('scheduledapp/', scheduledapp, name='scheduledapp'),
    ]