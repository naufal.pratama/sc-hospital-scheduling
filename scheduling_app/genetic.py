import random

def fitness_function(sequence):
    return 1

def random_selection(population):
    pop_size = len(population)
    maxi = population[random.randint(1, pop_size) - 1]
    for i in range(5):
        temp = population[random.randint(1, pop_size) - 1]
        if fitness_function(temp) > fitness_function(maxi):
            maxi = temp
    return maxi

def reproduce(sequence1, sequence2):
    pivot = random.randint(1, 13)
    child = []
    for i in range(14):
        if i < pivot:
            child.append(sequence1[i])
        else:
            child.append(sequence2[i])
    return child

def get_random_sequence(hosp_length):
    first_shift = False
    last_shift = False
    out_seq = []
    for i in range(14):
        if last_shift:
            out_seq.append(0)
            last_shift = False
        elif i != 13:
            if random.randint(1, 2) == 1:
                out_seq.append(random.randint(1, hosp_length))
                last_shift = True
                if i == 1:
                    first_shift = True
            else:
                out_seq.append(0)
        elif not first_shift:
            out_seq.append(random.randint(1, hosp_length))
        else:
            out_seq.append(0)
    return out_seq
    
def genetic_algorithm(population, hospitals, n):
    pop_size = len(population)
    hosp_length = len(hospitals)
    for i in range(n):
        new_population = []
        for j in range(pop_size):
            sequence1 = random_selection(population)
            sequence2 = random_selection(population)
            child = reproduce(sequence1, sequence2)

            # Mutation chance 1%
            if random.randint(1, 100) == 1:
                child = get_random_sequence(hosp_length)

            new_population.append(child)

        population = new_population
    return population

def test_algorithm():
    hospitals = ['H1', 'H2', 'H3', 'H4', 'H5']
    init_pop = []
    for i in range(10):
        init_pop.append(get_random_sequence(5))
    print('Before:')
    for seq in init_pop:
        print(seq)
    init_pop = genetic_algorithm(init_pop, hospitals, 1)
    print('After:')
    for seq in init_pop:
        print(seq)
